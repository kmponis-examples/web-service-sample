# WEB-SERVICE-SAMPLE
## Prerequisites: Deploy PARENT-POM-SAMPLE 
#### Download code
FROM alpine/git
WORKDIR /parent-pom
RUN git clone https://gitlab.com/kmponis-examples/parent-pom-sample.git
#### Build code
FROM maven:3.5-jdk-8-alpine
WORKDIR /parent-pom-build
COPY --from=0 /parent-pom/parent-pom-sample .
RUN mvn install

## Deploy: WEB-SERVICE-SAMPLE
#### Download code
FROM alpine/git
WORKDIR /web-service
RUN git clone https://gitlab.com/kmponis-examples/web-service-sample.git
#### Build code
FROM maven:3.5-jdk-8-alpine
WORKDIR /web-service-build
COPY --from=2 /web-service/web-service-sample .
RUN mvn install
#### Deploy code
FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=3 /web-service-build/target/web-service-sample.jar .
CMD ["java -jar web-service-sample.jar"]
#### ERROR: Due to downloading 'parent-pom-sample-1.0.0.pom' from central repo
######## Step 13/17 : RUN mvn install
########  ---> Running in 5034cce28bbd
######## [INFO] Scanning for projects...
######## Downloading from central: https://repo.maven.apache.org/maven2/com/parent/pom/parent-pom-sample/1.0.0/parent-pom-sample-1.0.0.pom
######## \[ERROR] [ERROR] Some problems were encountered while processing the POMs:
######## [FATAL] Non-resolvable parent POM for com.web.service.sample:web-service-sample:0.0.1-SNAPSHOT: Could not find artifact com.parent.pom:parent-pom-sample:pom:1.0.0 in central (https://repo.maven.apache.org/maven2) and 'parent.relativePath' points at wrong local POM @ line 13, column 10


# Other Examples
## Multi-stage build setup (https://docs.docker.com/develop/develop-images/multistage-build/)
#### Stage 1 (to create a "build" image, ~140MB)
FROM openjdk:8-jdk-alpine3.7 AS builder
RUN java -version
COPY . /usr/src/myapp/
WORKDIR /usr/src/myapp/
RUN apk --no-cache add maven && mvn --version
RUN mvn package
#### Stage 2 (to create a downsized "container executable", ~87MB)
FROM openjdk:8-jre-alpine3.7
WORKDIR /root/
COPY --from=builder /usr/src/myapp/target/app.jar .
EXPOSE 8123
ENTRYPOINT ["java", "-jar", "./app.jar"]

## SIMPLE-JAVA-MAVEN-APP
#### Download code
FROM alpine/git
WORKDIR /app
RUN git clone https://github.com/jenkins-docs/simple-java-maven-app.git
#### Build code
FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY --from=0 /app/simple-java-maven-app /app
RUN mvn install
#### Deploy code
FROM openjdk:8-jre-alpine
WORKDIR /app
#######COPY --from=1 /app/target/my-app-1.0-SNAPSHOT.jar /app
CMD ["java -jar my-app-1.0-SNAPSHOT.jar"]
