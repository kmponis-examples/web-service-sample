# Images
## cloudstack-fizzbuzz.ova
Install VM and docker. Get VM IP address(vm\_enp0s3\_ip) and apply it on: src/main/resources/application.properties, src/test/resources/application.properties and pom.xml

# Hypervisors
##Oracle VirtualBox:
* Download and install: https://www.virtualbox.org/wiki/Downloads
* Download fizzbuzz: http://dl.rohityadav.cloud/fizzbuzz/cloudstack-fizzbuzz.ova
* Start virtualbox: 
<br>- Import and run VM appliance(Ubuntu 18.04) 'cloudstach-fizzbuzz.ova'
<br>- Login(root:password) and copy the IP address for enp0s3(vm\_enp0s3\_ip): 192.168.*.* 
* Test Manually on CLI:
<br>`> ssh root@<vm_enp0s3_ip>`

# CI/CD
#### Docker  
* Open CLI and 'ssh root@<vm\_enp0s3\_ip>':
<br>`> sudo apt update`
<br>`> sudo apt install apt-transport-https ca-certificates curl software-properties-common`
<br>- Add the Kubernetes signing key
<br>`> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
<br>- Add docker
<br>`> sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`
<br>`> sudo systemctl status docker` // Display docker.service logs
<br>- Add -H=tcp://0.0.0.0:5555 at the end of ExecStart=
<br>`> sudo nano /lib/systemd/system/docker.service`
<br>`> systemctl daemon-reload` 
<br>`> sudo service docker restart`
<br>`> sudo docker run hello-world` // Run hello-world from library
<br>- Add docker-ce
<br>`> sudo apt update`
<br>`> apt-cache policy docker-ce`
<br>`> sudo apt install docker-ce`
<br>`> sudo dockerd-ce --help`
<br>- Add docker-compose
<br>`> sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
<br>`> sudo chmod +x /usr/local/bin/docker-compose`
<br>`> docker-compose --version`
* Test Manually on CLI (not connect to server):
<br>`> curl http://<vm_enp0s3_ip>:5555/images/json`
<br>`> curl http://<vm_enp0s3_ip>:5555/containers/json`

#### gitlab-runner - https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
* Open CLI and 'ssh root@<vm\_enp0s3\_ip>':
<br>`> sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`
<br>`> sudo chmod +x /usr/local/bin/gitlab-runner`
<br>- Create a GitLab CI user:
<br>`> sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`
<br>- Install and run as service:
<br>`> sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner`
<br>`> sudo gitlab-runner start`
<br>- Update with latest
<br>`> sudo gitlab-runner stop` 
<br>`> sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64`
<br>`> sudo chmod +x /usr/local/bin/gitlab-runner`
<br>`> sudo gitlab-runner start`
<br>-TODO: Connect with repository 
<br>`> sudo gitlab-runner register -n --url https://gitlab.com/ --registration-token REGISTRATION_TOKEN --executor shell --description "My Runner"`


#### Kubernetes
* Open CLI and 'ssh root@<vm\_enp0s3\_ip>':
<br>`> sudo snap install microk8s --classic`
<br>`> microk8s.kubectl get all --all-namespaces`
<br>`> microk8s.kubectl get no`
<br>`> microk8s.enable dns dashboard`
<br>`> `
