# Deploy Locally 
* Prerequisites: 
<br>- Install git, mvn, JDK8 and MongoDB
<br>- Deploy parent-pom-sample: https://gitlab.com/kmponis-examples/parent-pom-sample.git
* Open CLI and move to your workspace: 
<br>`> git clone https://gitlab.com/kmponis-examples/web-service-sample.git`
* Open CLI and move to web-service-sample: 
<br>`> mvn clean install`
<br>`> java -jar target/web-service-sample.jar`
* Test Manually on browser:
<br>`> http://localhost:8880/swagger-ui.html#/sample-controller/`
* Test Automatically (Jacoco code coverage (100%)):
<br>- Open CLI and move to web-service-sample:
<br>`> mvn clean test`
<br>- Go to file explorer on directory:
<br>`C:/**/web-service-sample/target/jacoco-reports/index.html`

# Deploy on Docker
## Using Dockerfile
* Prerequisites: 
<br>- Set up docker on host machine and create VM's with docker-machine(copy VM IP address).
* Open CLI
<br>`> docker build -t webservicesample -f Dockerfile .`
<br>`> docker tag webservicesample kbonis/web-service-sample:latest`
<br>`> docker login`
<br>`> docker push kbonis/web-service-sample:latest`
<br>`> docker stack deploy -c docker-compose.yml webservicelab`  

## Using Maven
* Prerequisites: 
<br>- Set up VM and docker and copy VM IP address(vm\_enp0s3\_ip).
* Open CLI and 'ssh root@<vm\_enp0s3\_ip>'
<br>- Pull and run mongoDB:
<br>`> sudo docker pull mongo`
<br>`> sudo docker images`
<br>`> sudo docker run -p 27017:27017 -t mongo`
* Exit from server and move to web-service-sample:
<br>`> mvn clean package docker:build`
* Open CLI and 'ssh root@<vm\_enp0s3\_ip>'
<br>`> sudo docker images`
<br>`> sudo docker run -p 8880:8880 -t web-service-sample`
* Test Manually on browser:
<br>`> http://<vm_enp0s3_ip>:8880/swagger-ui.html#/sample-controller/`
