# Images
## ubuntu-18.04.2-desktop-amd64.iso
* Verify your download and get OK: 
<br>`> echo "22580b9f3b186cc66818e60f44c46f795d708a1ad86b9225c458413b638459c4 *ubuntu-18.04.2-desktop-amd64.iso" | sha256sum --check `


# Hypervisors
## Virt Manager:
* Prerequisites: Brew
<br>- Install 
<br>`> /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install )"`
<br>- Uninstall
<br>`> ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall )"`
<br>- Install virt-manager repository
<br>`> brew tap jeffreywildman/homebrew-virt-manager`
* Install:
<br>- Search brew (jeffreywildman/virt-manager/virt-manager) 
<br>`> brew search`
<br>- Install rustup due to brew install delays 
<br>`> curl https://sh.rustup.rs -sSf | sh`
<br>- Install virt manager and virt viewer 
<br>`> brew install virt-manager virt-viewer`
* Import/run (Create QCOW2):
<br>- If qemu-img does not exists 
<br>`> brew install qemu`
<br>- Extract .ova and go to that folder 
<br>`> cd cloudstack-fizzbuzz`
<br>- Convert .vmdk into .qcow2: 
<br>`> qemu-img convert -O qcow2 cloudstack-fizzbuzz-disk001.vmdk cloudstack-fizzbuzz.qcow2`