package com.web.service.sample.controller.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ThreadControllerTest {
	private RestTemplate restTemplate;
	private HttpHeaders httpHeaders;

	private String baseUrl;
	private String threadScheduledExecutorServiceUrl;

	@Before
	public void setUp() throws Exception {
		restTemplate = new RestTemplate();
		httpHeaders = new HttpHeaders();

		baseUrl = "http://localhost:8980";
		threadScheduledExecutorServiceUrl = "/threadScheduledExecutorService";
	}

	// TODO for every exception
	@Ignore
	@Test(expected = HttpServerErrorException.class)
	public void givenNegativeCount_whenSaveSampleWithoutUsername_thenAssertObjectNotSaved() {
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
		parameters.add("count", "-1");
		parameters.add("corePoolSize", "5");
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters,
				httpHeaders);
		restTemplate.postForObject(baseUrl + threadScheduledExecutorServiceUrl, request, ResponseEntity.class);
	}

	// TODO for count [0,1,2,3,4,5,6]
	@Ignore
	@Test
	public void given0Count_whenSaveSampleWithoutUsername_thenAssertObjectNotSaved() {
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		String jsonSample = "{\"count\":0, \"corePoolSize\":4}";
		HttpEntity<String> httpEntity = new HttpEntity<String>(jsonSample, httpHeaders);
		String response = restTemplate.postForObject(baseUrl + threadScheduledExecutorServiceUrl, httpEntity, String.class);

		String expectedResponse = "----- STATUS ACCEPTED -----\r\n" + "Count:0 - CorePoolSize:4\r\n"
				+ "** Main thread START\r\n" + "- Notify main thread when CountDownLatch(0)\r\n" + "** Main thread WAIT\r\n"
				+ "- Notify main thread when CountDownLatch(1), (Line2 (2s))\r\n"
				+ "- Notify main thread when CountDownLatch(2), (Line3 (3s), Sum of 10, 4 loops)\r\n" + "** Main thread STOP";
		assertTrue(expectedResponse.contains(response));
	}

}
