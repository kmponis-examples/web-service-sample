package com.web.service.sample.service.impl;

import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.web.service.sample.model.ThreadScheduledExecutor;
import com.web.service.sample.service.ThreadService;

@Component
public class ThreadServiceImpl implements ThreadService {

	volatile int count = 0;
	volatile int corePoolSize = 0;
	volatile StringBuilder responseEntityLogsBuilder = new StringBuilder();

	@Override
	public ResponseEntity<String> threadScheduledExecutorService(ThreadScheduledExecutor threadScheduledExecutor) {
		count = threadScheduledExecutor.getCount();
		corePoolSize = threadScheduledExecutor.getCorePoolSize();

		System.out.println("\nScheduling task to run after 2, 3, 4, 5 seconds... " + new Date());
		System.out.println("-------------------------------------------------------------------------------");
		System.out.println("CountDownLatch (" + count + "), determines the point of main thread notification");

		CountDownLatch countDownLatch = new CountDownLatch(count);

		try {
			/*
			 * Info: It's also possible to use scheduleAtFixedRate or scheduleAtFixedDelay
			 * to run the task repeatedly using the thread pool
			 */
			System.out.println("ScheduledExecutorService (" + corePoolSize
					+ "), corePoolSize determines the number of threads in each pool");
			ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(corePoolSize);

			System.out.println("---------------------------------------------------------------------------------------");
			System.out.println("** Main thread START");
			responseEntityLogsBuilder.append("- Notify main thread when CountDownLatch(0)\n");
			responseEntityLogsBuilder.append("\n----- STATUS ACCEPTED -----\nCount:" + count + " - CorePoolSize:"
					+ corePoolSize + "\n** Main thread START\n");
			System.out.println("- Notify main thread when CountDownLatch(0)");
			responseEntityLogsBuilder.append("- Notify main thread when CountDownLatch(0)\n");
			scheduledExecutorService.schedule(new threadDisplayMessage("Line1 (5s)", countDownLatch), 5, TimeUnit.SECONDS);
			scheduledExecutorService.schedule(new threadDisplayMessage("Line2 (2s)", countDownLatch), 2, TimeUnit.SECONDS);
			threadCalculateSum line3 = new threadCalculateSum("Line3 (3s)", countDownLatch, 1l);
			scheduledExecutorService.schedule(line3, 3, TimeUnit.SECONDS);
			scheduledExecutorService.schedule(new threadDisplayMessage("Line4 (5s)", countDownLatch), 5, TimeUnit.SECONDS);
			scheduledExecutorService.schedule(new threadDisplayMessage("Line5 (5s)", countDownLatch), 5, TimeUnit.SECONDS);
			scheduledExecutorService.schedule(new threadDisplayMessage("Line6 (4s)", countDownLatch), 4, TimeUnit.SECONDS);

			try {
				countDownLatch.await();
				System.out.println("CallerName[" + Thread.currentThread().getName() + "] - Sum: " + line3.getSum());
			} catch (InterruptedException e) {
				e.printStackTrace();
				responseEntityLogsBuilder.append("** Main thread InterruptedException\n");
				return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(responseEntityLogsBuilder.toString());
			} finally {
				scheduledExecutorService.shutdown();
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			responseEntityLogsBuilder.append("** Main thread IllegalArgumentException\n");
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(responseEntityLogsBuilder.toString());
		}

		if (4 <= count) {
			System.out.println("- Notify main thread when CountDownLatch(4+)");
			responseEntityLogsBuilder.append("- Notify main thread when CountDownLatch(4+)\n");
		}
		System.out.println("** Main thread STOP");
		responseEntityLogsBuilder.append("** Main thread STOP\n");
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseEntityLogsBuilder.toString());
	}

	/**
	 * Inner class for calculating sum of loop 5. Sleep, wait and notify main thread
	 * with countDownLatch.countDown() and notifyAll()
	 */
	class threadCalculateSum implements Runnable {
		private CountDownLatch countDownLatch;
		private String callerName;
		private Long sleep;
		private volatile int sum;
		private boolean done = false;

		public threadCalculateSum(String callerName, CountDownLatch countDownLatch, Long sleep) {
			this.callerName = callerName;
			this.countDownLatch = countDownLatch;
			this.sleep = sleep;
		}

		@Override
		public void run() {
			for (int i = 1; i <= 5; i++) {
				System.out.println("CallerName[" + Thread.currentThread().getName() + "]: " + callerName + " - Message: " + i);
				this.sum += i;
				try {
					if (10 == this.sum) {
						countDownLatch.countDown();
						System.out.println("- Notify main thread when CountDownLatch(2), (Line3 (3s), Sum of 10, 4 loops)");
						responseEntityLogsBuilder
								.append("- Notify main thread when CountDownLatch(2), (Line3 (3s), Sum of 10, 4 loops)\n");
					}
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			done = true;
			synchronized (this) {
				this.notifyAll();
			}
		}

		public int getSum() throws InterruptedException {
			synchronized (this) {
				if (!done) {
					System.out.println("** Main thread WAIT");
					responseEntityLogsBuilder.append("** Main thread WAIT\n");
					this.wait();
				}
			}
			return this.sum;
		}
	}

	/**
	 * Inner class to display message. Notify main thread with
	 * countDownLatch.countDown()
	 */
	class threadDisplayMessage implements Runnable {
		private String callerName;
		private CountDownLatch countDownLatch;

		public threadDisplayMessage(String callerName, CountDownLatch countDownLatch) {
			this.callerName = callerName;
			this.countDownLatch = countDownLatch;
		}

		@Override
		public void run() {
			System.out
					.println("CallerName[" + Thread.currentThread().getName() + "]: " + callerName + " - Message: " + new Date());
			if ("Line2 (2s)".equals(callerName)) {
				System.out.println("- Notify main thread when CountDownLatch(1), (Line2 (2s))");
				responseEntityLogsBuilder.append("- Notify main thread when CountDownLatch(1), (Line2 (2s))\n");
			} else if ("Line6 (4s)".equals(callerName)) {
				System.out.println("- Notify main thread when CountDownLatch(3), (Line6 (4s))");
				responseEntityLogsBuilder.append("- Notify main thread when CountDownLatch(3), (Line6 (4s))\n");
			}
			countDownLatch.countDown();
		}
	}

}
