package com.web.service.sample.service;

import org.springframework.http.ResponseEntity;

import com.web.service.sample.model.ThreadScheduledExecutor;

public interface ThreadService {

	/**
	 * Schedules a Runnable task using a ScheduledExecutorService.schedule(). Also,
	 * blocks the main thread until the countdown executions reaches <count>.
	 * 
	 * @param (ThreadScheduledExecutor) threadScheduledExecutor
	 * @return (ResponseEntity<String>)
	 */
	public ResponseEntity<String> threadScheduledExecutorService(ThreadScheduledExecutor threadScheduledExecutor);

}
