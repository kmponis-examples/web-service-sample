package com.web.service.sample;

import org.springframework.context.annotation.ComponentScan;

import com.web.service.sample.model.ThreadScheduledExecutor;
import com.web.service.sample.service.impl.ThreadServiceImpl;

@ComponentScan
public class HomeThread {

	/*
	 * CountDownLatch 'count' determines the point of main thread notification.
	 * ScheduledExecutorService 'corePoolSize' determines the number of threads in
	 * each pool.
	 */
	public static final int count = 0;
	public static final int corePoolSize = 5;

	public static void main(String[] args) {
		new ThreadServiceImpl().threadScheduledExecutorService(new ThreadScheduledExecutor(count, corePoolSize));
	}

}
