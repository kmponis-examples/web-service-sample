package com.web.service.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.web.service.sample.model.ThreadScheduledExecutor;
import com.web.service.sample.service.ThreadService;

import io.swagger.annotations.ApiOperation;

@RestController
public class ThreadController {

	@Autowired
	private ThreadService threadService;

	@ApiOperation(value = "Schedules a runnable task and blocks the main thread until the countdown equal to count", response = ResponseEntity.class)
	@PostMapping(value = "/threadScheduledExecutorService")
	public ResponseEntity<String> threadScheduledExecutorService(
			@RequestBody ThreadScheduledExecutor threadScheduledExecutor) {
		return threadService.threadScheduledExecutorService(threadScheduledExecutor);
	}

}
