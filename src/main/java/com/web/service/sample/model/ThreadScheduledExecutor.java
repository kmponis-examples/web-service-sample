package com.web.service.sample.model;

import lombok.Builder;
import lombok.Data;

/**
 * ThreadScheduledExecutor entity to post
 */
@Data
@Builder
public class ThreadScheduledExecutor {
	private int count;
	private int corePoolSize;

	public ThreadScheduledExecutor(int count, int corePoolSize) {
		this.count = count;
		this.corePoolSize = corePoolSize;
	}
}
